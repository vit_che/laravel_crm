<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/usersList', 'UserController@usersList')->name('usersList');

Route::get('/userData/{id}', 'UserController@userData')->name('userData');

Route::get('/formCall/{user_id}', 'CallController@formCall')->name('formCall');

Route::post('/addCall', 'CallController@addCall')->name('addCall');
