<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    public function usersList(){

        $users = User::all();

        $data = [
            'users' => $users,
        ];

        return view('users_list', $data);
    }

    public function userData(Request $request){

        $id = $request->id;

        $user = User::find($id);
        $calls = User::find($id)->calls;

        $data = [
            'user' => $user,
            'calls' => $calls
        ];

        return view('user_data', $data);
    }
}
