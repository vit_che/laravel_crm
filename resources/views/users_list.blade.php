


@extends('layout')

@section('userslist')

    <div class="text-center">
        <h1>Users List</h1>
    </div>


    <div class="container">

        <table class="table table-striped table-bordered table-hover table-sm">

            <thead class="text-center">
                <th>User_id</th><th>Description</th><th>Status</th>
            </thead>

            <tbody>
            @foreach( $users as $user)
                <tr class="table-info" onClick="location.href='{{ route('userData',['id'=>$user->id]) }}'">
                    <td>
                        {{ $user->id  }}
                    </td>

                    <td>
                        {{ $user->description }}
                    </td>

                    <td>
                        {{ $user->created_at }}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

    </div>
@endsection
