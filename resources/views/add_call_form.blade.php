


@extends('layout')

@section('add_call_form')

    <!-- return to main page -->
    <div class="text-center">
        <a href="{{ route('usersList') }}" class="btn btn-outline-primary" role="button" aria-pressed="true">Back to Users List</a>
    </div>

    <div class="text-center">
        <h5>Add Call Form</h5>
    </div>

    <div class="container">
        <!--   Add call Form     -->
        <form method="post" action="{{ route('addCall') }}" enctype="multipart/form-data">

            @csrf
            <!--   hidden input     -->
            <input type="text" class="form-control" id="inputUser_id" name="user_id" value="{{ $user_id }}" hidden>


            <div class="form-group">
                <label for="inputNumber">Call number</label>
                <input type="text" class="form-control" id="inputNumber" name="number" placeholder="Enter call number">
            </div>

            <button type="submit" class="btn btn-primary" style="margin-top: 50px">Add call</button>

        </form>

    </div>

@endsection
