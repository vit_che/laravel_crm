<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{

    protected $fillable = [
        'description', 'status',
    ];

    public function calls()
    {
        return $this->hasMany('App\Call');
    }

}
