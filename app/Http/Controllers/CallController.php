<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Call;

class CallController extends Controller
{
    public function formCall(Request $request){

        $user_id = $request->user_id;

        $data = [
            'user_id' => $user_id,
        ];

        return view('add_call_form', $data);
    }

    public function addCall(Request $request){

        $input = $request->except('_token');

        $call = new Call();

        $call->user_id = $input['user_id'];
        $call->number = $input['number'];

        $call->save();

        return redirect()->route('userData', ['id'=>$input['user_id']]);
    }
}
