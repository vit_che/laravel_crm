
@extends('layout')

@section('user_data')

    <div class="text-center">
        <a href="{{ route('usersList') }}" class="btn btn-outline-primary" role="button" aria-pressed="true">Back to Users List</a>
    </div>

    <div class="text-center">
        <h5>User History</h5>
    </div>

    <div class="container">

        <table class="table table-striped table-bordered table-hover table-sm">
            <thead class="text-center">
                <th>User_id</th><th>Description</th><th>Status</th>
            </thead>
            <thead class="text-center">
                <th>{{ $user->id }}</th><th>{{$user->description }}</th><th>{{ $user->status }}</th>
            </thead>
        </table>


        <div class="text-center">
            <a href="{{ route('formCall',['user_id'=>$user->id]) }}" class="btn btn-outline-primary" role="button" aria-pressed="true">Add Call</a>
            <h5>Users Calls History</h5>
        </div>

        <table class="table table-striped table-bordered table-hover table-sm">

            <thead class="text-center">
                <th>call_id</th><th>number</th><th>date</th>
            </thead>

            <tbody>
            @foreach( $calls as $call)
                <tr class="table-info text-center">
                    <td>
                        {{ $call->id }}
                    </td>

                    <td>
                        {{ $call->number }}
                    </td>

                    <td>
                        {{ $call->created_at }}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

    </div>

@endsection
